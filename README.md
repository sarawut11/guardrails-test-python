# guardrails-test-python

[![GuardRails badge](https://badges.production.guardrails.io/guardrailsio/guardrails-test-python.svg?token=164601d720686992a3ff26713142f2d3b8d02bf8920e2f91c83e93161cd96963&ts=1535856114563)](https://www.guardrails.io)

**Test repository for Python-based SAST tools**

## Sources

1. https://github.com/mpirnat/lets-be-bad-guys
2. https://github.com/nVisium/django.nV
3. https://github.com/stamparm/DSVW
4. https://github.com/anxolerd/dvpwa
