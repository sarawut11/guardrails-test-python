import sys
import logging
from Crypto.PublicKey import DSA

from aiohttp.web import run_app

from sqli.app import init as init_app

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    DSA.generate_private_key(512, backends.default_backend())

    app = init_app(sys.argv[1:])

    run_app(app,
            host=app['config']['app']['host'],
            port=app['config']['app']['port'])
